FROM python:latest

WORKDIR /

COPY . .

ENV PYTHONUNBUFFERED=1
ENV DATABASE_PATH=/data/state.db
ENV BUILD_PATH=/contracts/build/contracts/

RUN pip install -r deployment/requirements.txt
RUN pip install -r oracle/requirements.txt

CMD python /oracle/main.py
