import brownie
import pytest


@pytest.fixture()
def validator(a, BridgeValidators):
    yield a[0].deploy(BridgeValidators, [a[1].address], 1)


@pytest.fixture()
def bridge1(accounts, validator, NativeTokenBridge):
    b = accounts[0].deploy(NativeTokenBridge, validator.address, False)
    yield b


@pytest.fixture()
def bridge2(accounts, validator, NativeTokenBridge):
    b2 = accounts[0].deploy(NativeTokenBridge, validator.address, True)
    yield b2


def test_ac_010_01(fn_isolation, a, bridge1, bridge2, web3):
    bridge1.addLiquidity({"from": a[0], "value": "100 ether"})
    a[6].transfer(a[0], '100 ether')
    bridge2.addLiquidity({"from": a[0], "value": "100 ether"})
    a[2].transfer(bridge1.address, amount="100 ether")
    bridge2.commit(
        a[2].address, 100e18,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[1]}
    )
    a[3].transfer(bridge2.address, amount="10 ether")
    bridge1.commit(
        a[3].address, 10e18,
        "0xddfbb81ca5f813ff658bc60df9d23a464cec78966b90d98aa9c09ce4045bb285",
        {"from": a[1]}
    )
