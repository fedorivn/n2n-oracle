import os
import dotenv
from brownie import accounts, TokenPrices, network

from .gen_events import generate_events
from .deploy_dest import after_deploy_for_dest

UPDATE_DOTENV = False


def main():
    if network.show_active() != 'development':
        raise RuntimeError('This script can only be run in development network')

    dotenv_path = os.getcwd() + '/.env'
    print(dotenv_path)
    # values = dotenv.dotenv_values(dotenv_path)
    contract = TokenPrices
    deployer = accounts[0]

    source = deployer.deploy(contract)
    dest = deployer.deploy(contract)
    after_deploy_for_dest(deployer, dest)

    sender = accounts[0]
    generate_events(source, sender, 10)

    rpc_url = 'http://localhost:8545'
    params = [
        ('SOURCE_ADDRESS', source.address),
        ('DESTINATION_ADDRESS', dest.address),
        ('SOURCE_RPCURL', rpc_url),
        ('DESTINATION_RPCURL', rpc_url)
    ]

    print('Copy and paste this values in your root .env file')
    for key, value in params:
        print(f'{key}={value}')

    if UPDATE_DOTENV:
        dotenv_path = os.getcwd() + '/../.env'
        for key, value in params:
            dotenv.set_key(dotenv_path, key, value)
