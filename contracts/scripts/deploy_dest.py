import os

import dotenv
from brownie import accounts, network

from .deploy import main as deploy_contract


def after_deploy_for_dest(deployer, contract):
    print('Starting after deployment settings...')
    path_to_root_dotenv = os.getcwd() + '/../.env'
    parsed = dotenv.dotenv_values(path_to_root_dotenv)
    oracle = accounts.add(private_key=parsed['PRIVKEY'])

    print('Set oracle address from root .env file')
    contract.changeOracle(oracle.address, {'from': deployer})

    if network.show_active() == 'development':
        print('Transfer some tokens to oracle address')
        deployer.transfer(oracle, '1 ether')


def main():
    deployed = deploy_contract()
    parsed = dotenv.dotenv_values(dotenv.find_dotenv(usecwd=True))
    after_deploy_for_dest(parsed['DEPLOYER_PRIVATE_KEY'], deployed)
