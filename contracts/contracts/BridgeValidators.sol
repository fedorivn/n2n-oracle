// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.5;

contract BridgeValidators {
    
    uint public threshold;
    mapping (address => bool) public isValidator;
    address[] public validators;
    address admin;
    
    
    modifier isAdmin() {
        require(msg.sender == admin);
        _;
    }
    
    modifier valitatorDoesNotExist(address validator) {
        require(!isValidator[validator]);
        _;
    }
    
    modifier valitatorExist(address validator) {
        require(isValidator[validator]);
        _;
    }
    
    modifier validatorsdAmountUpdateTresholdCheck() {
        require(validators.length-1 >= threshold);
        _;
    }
    
    modifier thresholdUpdateCheck(uint thresh) {
        require(validators.length >= thresh);
        require(thresh > 0);
        _;
    }
    
    constructor(address[] memory _validators, uint256 _threshold)
    {
        admin = msg.sender;
        threshold = _threshold;
        validators = _validators;
        for (uint i=0; i<validators.length; i++) {
            require(validators[i] != address(0));
            isValidator[validators[i]] = true;
        }
    }
    
    function addValidator(address newvalidator) public
        isAdmin()
        valitatorDoesNotExist(newvalidator)
    {
        isValidator[newvalidator] = true;
        validators.push(newvalidator);
    }
    
    function removeValidator(address validator) public
        isAdmin()
        valitatorExist(validator)
        validatorsdAmountUpdateTresholdCheck()
    {
        delete isValidator[validator];
        for (uint i=0; i<validators.length; i++) {
            if (validators[i] == validator) {
                validators[i] = validators[validators.length - 1];
                validators.pop();
                break;
            }
        }
    }
    
    function changeThreshold(uint256 thresh) public
        isAdmin()
        thresholdUpdateCheck(thresh)
    {
        threshold = thresh;
    }
    
    function getValidators() public view returns (address [] memory) {
        return validators;
    }
    function getThreshold() public view returns (uint) {
        return threshold;
    }
}
