// SPDX-License-Identifier: None

pragma solidity >=0.7.5;

contract selfDestructContract {

    address owner;
    address payable recipient;

    constructor(address payable _recipient) {
        owner = msg.sender;
        recipient = _recipient;
    }

    receive() external payable {

    }

    function destroy() public payable {
        require(msg.sender == owner);
        selfdestruct(recipient);
    }
}
