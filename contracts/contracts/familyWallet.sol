pragma solidity 0.7.5;

    contract familyWallet {
        event Received(address sender, uint256 value);
        address owner1;
        address owner2;

        constructor (address husband, address wife) {
            require(husband != wife, "the same");
            require(wife != address(0), "is zero");

            owner1 = husband;
            owner2 = wife;
        }

        function sendFunds(address payable receiver, uint256 value) external {
            require(msg.sender == owner1 || msg.sender == owner2, "Not allowed");
            require(value <= address(this).balance, "not enough");
            receiver.transfer(value);
        }

        receive () payable external {
            emit Received(msg.sender, msg.value);
        }
    }