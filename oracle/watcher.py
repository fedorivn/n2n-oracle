from typing import NamedTuple, Any, Callable
import asyncio
from logging import getLogger

from web3 import Web3

logger = getLogger("oracle.watcher")

FETCH_ONLY_OLD_EVENTS = 1
FETCH_ONLY_NEW_EVENTS = 2
FETCH_ALL_EVENTS = 3


class EventProcessor(NamedTuple):
    filter: Any
    handler_func: Callable
    # FETCH_ONLY_OLD_EVENTS or
    # FETCH_ONLY_NEW_EVENTS or
    # FETCH_ALL_EVENTS
    mode: int
    many_at_once: bool


class ContractEventsWatcher:
    """
    Handles the events emitted by the contract
    """

    def __init__(self, w3: Web3, contract=None, address=None, abi=None,
                 poll_interval=5, from_block_number: int = 'latest'):
        self.w3 = w3

        if not any([contract, address, abi]):
            raise ValueError

        if contract:
            self.contract = contract
        else:
            self.contract = self.w3.eth.contract(address, abi=abi)

        self.poll_interval = poll_interval
        self.from_block = from_block_number
        self.events_processors = []
        self.post_poll_handlers = []

    async def run_polling(self):
        """
        Starts poll of the contract.
        At first, it makes an initial poll to handle all already emitted events.
        :return: None
        """
        initial = True

        while True:
            await self._poll(initial=initial)
            initial = False

            logger.debug(
                f"sleeping for {self.poll_interval} seconds after poll"
            )
            await asyncio.sleep(self.poll_interval)

    async def _poll(self, initial=False):
        logger.debug(f"polling, initial={initial}")

        if initial:
            modes = [FETCH_ONLY_OLD_EVENTS, FETCH_ALL_EVENTS]
        else:
            modes = [FETCH_ONLY_NEW_EVENTS, FETCH_ALL_EVENTS]
        processors = filter(lambda p: p.mode in modes, self.events_processors)

        for processor in processors:
            f = processor.filter
            entries = await asyncio.to_thread(
                f.get_all_entries if initial else f.get_new_entries)
            logger.debug(f"recevied {len(entries)} events")
            if len(entries) == 0:
                continue

            if processor.many_at_once:
                asyncio.create_task(processor.handler_func(entries))
                continue

            for event in entries:
                asyncio.create_task(processor.handler_func(event))

        logger.debug(f"starting post-poll handlers")
        for handler in self.post_poll_handlers:
            asyncio.create_task(handler())

    def add_event_processor(
        self,
        event_name: str,
        handler_func,
        argument_filters: dict = None,
        mode=FETCH_ALL_EVENTS,
        many_at_once: bool = False,
    ):

        try:
            event_class = self.contract.events[event_name]
        except ValueError:
            raise ValueError(
                f"Incorrect event name. Event {event_name} not found in contract's abi."
            )

        event_filter = event_class.createFilter(
            fromBlock=self.from_block,
            argument_filters=argument_filters
        )
        event_processor = EventProcessor(
            filter=event_filter,
            handler_func=handler_func,
            mode=mode,
            many_at_once=many_at_once,
        )
        self.events_processors.append(event_processor)

    def event_handler(self, event_name: str, argument_filters: dict = None,
                      mode=FETCH_ALL_EVENTS, many_at_once: bool = False):
        """
        Метод служит декоратором для ваших функций. После того, как вы вешаете декоратор
        на функцию, она будет исполнятся всякий раз, когда будет инициировано новое событие.

        :param event_name: Имя ивента (как в abi контракта)
        :param argument_filters: Словарь аргументов, если нужно фильтровать ивенты
        :param mode: меняет поведение
        :return: Декорированная функция
        """
        def decorator(handler_func):
            self.add_event_processor(
                event_name, argument_filters, handler_func, mode, many_at_once
            )
            return handler_func

        return decorator

    def post_poll(self, handler: Callable):
        self.post_poll_handlers.append(handler)
